\section{}

\subsection{}

\begin{task}
    Prove that the function
    \[
    f \colon \RR \to \RR, \qquad x \mapsto e^{-\pi x^2}
    \]
    is a Schwartz function.
\end{task}

\begin{proof}
    First of all the function $f$ is clearly smooth.
    Now we note that for a polynomial $p(x) \in \RR[x]$ the derivative of the function
    \[
    \RR \to \RR, \qquad x \mapsto p(x) e^{-\pi x^2}
    \]
    is given by
    \[
    x \mapsto (p'(x) - 2 \pi x p(x)) e^{-\pi x^2}.
    \]
    By induction it follows that the derivatives $f^{(n)}$ for $n \in \ZZ_{\geq 0}$ of $f$ are all of the form $p(x) e^{-\pi x^2}$ for some $p(x) \in \RR[x]$.
    As the rapidly decreasing functions are stable under multiplication with $x$ and stable under taking $\RR$-linear combinations, they are stable under multiplication with arbitrary polynomials and we are reduced to showing that $f$ itself is rapidly decreasing.
    
    For this we note that $e^{-\pi x^2} \leq e^{-\pi x}$ for $x \in \RR_{\geq 1}$ so that $f$ is rapidly decreasing for $t \to \infty$.
    By symmetry it is also rapidly decreasing for $t \to -\infty$.
\end{proof}


\subsection{}

\begin{task}
    Show that $\widehat{f} = f$.
\end{task}

\begin{proof}
    For $x \in \RR$ we have
    \begin{align*}
        \widehat{f}(x) &= \int_{\RR} f(y) e^{-2 \pi i x y} \, dy \\
        &= \int_{\RR} e^{-\pi y^2 - 2 \pi i x y} \, dy \\
        &= e^{- \pi x^2} \int_{\RR} e^{-\pi (y + ix)^2} \, dy.
    \end{align*}
    Hence it suffices to show that the integral ocurring in the last line is equal to $1$.
    For this, fix a constant $R \in \RR_{> 0}$, define paths
    \begin{align*}
        \gamma_1 &\colon [-R, R] \to \CC, \qquad t \mapsto t \\
        \gamma_2 &\colon [-R, R] \to \CC, \qquad t \mapsto t + ix \\
        \gamma_3 &\colon [0, x] \to \CC, \qquad t \mapsto R + it \\
        \gamma_4 &\colon [0, x] \to \CC, \qquad t \mapsto -R + it
    \end{align*}
    and consider the holomorphic function
    \[
    g \colon \CC \to \CC, \qquad z \mapsto e^{- \pi z^2}.
    \]
    We make the following observations:
    \begin{itemize}
        \item
        We have
        \[
        \lim_{R \to \infty} \int_{\gamma_1} g(z) \, dz = \int_{\RR} e^{-\pi t^2} \, dt.
        \]
        
        \item
        Similarly we have
        \[
        \lim_{R \to \infty} \int_{\gamma_2} g(z) \, dz = \int_{\RR} e^{-\pi (t + ix)^2} \, dt.
        \]
        This is precisely what we want to compute.
        
        \item
        The four paths together make up a rectangle.
        As the integral of a holomorphic function along a path only depends on the homotopy class (relative to the endpoints) of that path we obtain
        \[
        \int_{\gamma_1} g(z) \, dz + \int_{\gamma_3} g(z) \, dz = \int_{\gamma_2} g(z) \, dz + \int_{\gamma_4} g(z) \, dz.
        \]
        
        \item
        We claim that
        \[
        \lim_{R \to \infty} \int_{\gamma_3} g(z) \, dz = 0
        \]
        and likewise for $\gamma_4$.
        By symmetry it suffices to consider $\gamma_3$.
        Now for $t \in [0, x]$ we have
        \[
        \abs{g(\gamma_3(t))} = e^{-\pi \Re((R + it)^2)} = e^{-\pi (R^2 - t^2)} \leq e^{-\pi (R^2 - x^2)}
        \]
        so that we obtain
        \[
        \abs[\Bigg]{\int_{\gamma_3} g(z) \, dz} \leq \abs{x} e^{-\pi (R^2 - x^2)}.
        \]
        This bound clearly converges to $0$ for $R \to \infty$.
    \end{itemize}
    
    Together these observations imply that
    \begin{align*}
        \int_{\RR} e^{-\pi (t + ix)^2} \, dt &= \lim_{R \to \infty} \int_{\gamma_2} g(z) \, dz + \int_{\gamma_4} g(z) \, dz \\
        &= \lim_{R \to \infty} \int_{\gamma_1} g(z) \, dz + \int_{\gamma_3} g(z) \, dz \\
        &= \int_{\RR} e^{-\pi t^2} \, dt = 1. \qedhere
    \end{align*}
\end{proof}


\subsection{}

\begin{task}
    For $t \in \RR_{>0}$ define
    \[
    f_t \colon \RR \to \RR, \qquad x \mapsto e^{-\pi t x^2}.
    \]
    Show that $\widehat{f_t} = t^{-1/2} f_{1/t}$.
\end{task}

\begin{proof}
    For $x \in \RR$ we have
    \begin{align*}
        \widehat{f_t}(x) &= \int_{\RR} e^{-\pi t y^2 - 2 \pi i x y} \, dy \\
        &= e^{-\pi t^{-1} x^2} \int_{\RR} e^{-\pi (t^{1/2} y + i t^{-1/2} x)^2} \, dy
    \end{align*}
    Moreover we can compute (using a linear change of variables and the proof of the second part of the exercise)
    \[
    \int_{\RR} e^{-\pi (t^{1/2} y + i t^{-1/2} x)^2} \, dy = t^{-1/2} \int_{\RR} e^{-\pi (y + i t^{-1/2} x)^2} \, dy = t^{-1/2}. \qedhere
    \]
\end{proof}
