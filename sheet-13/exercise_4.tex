\section{}

Let $(G_i)_{i \in I}$ be a family of locally compact abelian groups and $(H_i)_{i \in I}$ a family of compact open subgroups $H_i \subseteq G_i$.
We define $G \coloneqq \prod_{i \in I} (G_i, H_i)$.

\subsection{}

\begin{task}
    Let $\chi_i \in G_i^{\vee}$ be characters such that $\chi_i(H_i) = \curlybr{1}$ for all but finitely many $i \in I$.
    Check that $\chi = \prod_{i \in I} \chi_i$ is a well-defined character of $G$.
\end{task}

\begin{proof}
    To see that $\chi$ is well-defined as a map of sets we note that for each $g = (g_i)_{i \in I} \in G$ we have $g_i \in H_i$ for all but finitely many $i$ and consequently $\chi_i(g_i) = 1$ for all but finitely many $i \in I$.
    Thus the product in the definition of $\chi$ is actually finite so that the definition makes sense.
    It is now clear that $\chi$ is also a well defined map of groups so that it remains to check that it is continuous.

    For this it suffices to show that the restriction of $\chi$ to the open subgroup $H \coloneqq \prod_{i \in I} H_i \subseteq G$ is continuous.
    But this is true because $\chi|_H$ is actually a finite product of characters (because all but finitely many of the $\chi_i$ restrict to the trivial character on $H_i$).
\end{proof}

\subsection{}

\begin{task}
    Conversely, show that for every character $\chi \in G^{\vee}$ there are (unique) characters $\chi_i \in G_i^{\vee}$ such that $\chi_i(H_i) = 1$ for all but finitely many $i \in I$ and $\chi = \prod_{i \in I} \chi_i$.
\end{task}

\begin{proof}
    Let $\chi \in G^{\vee}$ be given.
    Then we define characters $\chi_i$ by precomposing $\chi$ with the (continuous) \enquote{inclusions} $\psi_i \colon G_i \to G$ that are given by $\psi_i(g) = (\psi_i(g)_j)_{j \in I}$ with
    \[
        \psi_i(g)_j \coloneqq
        \begin{cases}
            g & j = i \\
            1 & j \neq i.
        \end{cases}
    \]
    Now we apply the standard trick (namely choosing an open neighborhood of $1$ in $\mathbf{T}$) to see that $\chi|_H$ factors over $\prod_{i \in J} H_i$ for some finite subset $J \subseteq I$.
    This now implies that $\chi_i(H_i) = \curlybr{1}$ for all but finitely many $i$ and that $\chi = \prod_{i \in I} \chi_i$ (where the right hand side is a well-defined character of $G$ by the first part of the exercise).

    The uniqueness of the $\chi_i$ is clear.
\end{proof}

\subsection{}

\begin{task}
    Prove that $G^{\vee} \cong \prod_{i \in I} \roundbr[\big]{G_i^{\vee}, (G_i/H_i)^{\vee}}$.
\end{task}

\begin{proof}
    Consider the map
    \[
        \prod_{i \in I} \roundbr[\big]{G_i^{\vee}, (G_i/H_i)^{\vee}} \to G^{\vee}, \qquad (\chi_i)_i \mapsto \prod_{i \in I} \chi_i.
    \]
    By the first part of the exercise this is well defined and by the second part of the exercise it is bijective.
    So we know that the map is an isomorphism of abstract abelian groups and it remains to check that it identifies the topologies on both sides.

    For this it suffices to find an open subgroup on each side such that the map restricts to an isomorphism of topological groups between these two subgroups.
    Now $G$ contains the compact open subgroup $H$ (see first part of the exercise for notation).
    Thus (again using the standard trick) the subgroup $U$ of all characters $\chi \in G^{\vee}$ with $\chi(H) = \curlybr{1}$ is open.
    Note that $G/H \cong \bigoplus_{i \in I} G_i/H_i$ (equipped with the discrete topology) so that this subgroup is precisely the image of the injective continuous map
    \[
        \prod_{i \in I} (G_i/H_i)^{\vee} \cong \roundbr[\Big]{\bigoplus_{i \in I} G_i/H_i}^{\vee} \to G^{\vee}.
    \] 
    The source of this map is compact so that the map actually defines an isomorphism of topological groups $\prod_{i \in I} (G_i/H_i)^{\vee} \to U$.
    But now we observe that the map agrees with the composition
    \[
        \prod_{i \in I} (G_i/H_i)^{\vee} \subseteq \prod_{i \in I} \roundbr[\big]{G_i^{\vee}, (G_i/H_i)^{\vee}} \to G^{\vee}
    \]
    so that we win because $\prod_{i \in I} (G_i/H_i)^{\vee} \subseteq \prod_{i \in I} \roundbr[\big]{G_i^{\vee}, (G_i/H_i)^{\vee}}$ is an open subgroup.
\end{proof}