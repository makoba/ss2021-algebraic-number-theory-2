\section{}

Let $S \subseteq T \subseteq \ZZ_{>0}$.

\subsection{}

\begin{task}
    Assume that the natural density of $S$ in $T$ exists and denote it by $\delta$.
    Also assume that we have $\sum_{k \in T} k^{-1} = \infty$.
    Prove that the Dirichlet density of $S$ in $T$ exists and that it is also $\delta$.
\end{task}

\begin{claim}
    Let $U \subseteq \ZZ_{>0}$.
    Define a function
    \[
        A_U \colon \RR_{\geq 1} \to \RR, \qquad x \mapsto \abs[\big]{\set[\big]{k \in U}{k \leq x}}.
    \]
    Then, for $s \in \RR_{\geq 1}$ we have
    \[
        \sum_{k \in U} k^{-s} = s \cdot \int_1^{\infty} \frac{A_U(x)}{x^{s+1}} \, dx.
    \]
\end{claim}

\begin{proof}
    We have $A_U = \sum_{k \in U} 1_{\curlybr{x \geq k}}$.
    Thus the claim follows from the computation
    \[
        s \cdot \int_k^{\infty} \frac{1}{x^{s+1}} \, dx = \squarebr[\Big]{-x^{-s}}_k^{\infty} = k^{-s}
    \]
    for $k \in \ZZ_{>0}$.
\end{proof}

Using the above claim we see that for $s \in \RR_{>1}$ we have
\[
    \frac{\sum_{k \in S} k^{-s}}{\sum_{k \in T} k^{-s}} = \delta + \frac{\int_1^{\infty} \frac{A_S(x) - \delta A_T(x)}{x^{s+1}} \, dx}{\int_1^{\infty} \frac{A_T(x)}{x^{s+1}} \, dx}
\]
so that it suffices to show that the second summand on the right hand side converges to $0$ for $s \to 1^+$.

Now let $\varepsilon \in \RR_{>0}$.
Then by assumption we have
\[
    \abs{A_S(x) - \delta A_T(x)} \leq \varepsilon A_T(x)
\]
for all large enough $x$.
Thus we find a constant $C \in \RR_{\geq 0}$ such that in fact
\[
    \abs{A_S(x) - \delta A_T(x)}  \leq \varepsilon A_T(x) + C
\]
for all $x \in \RR_{\geq 1}$.
It follows that for $s \in \RR_{>1}$ we have
\[
    \abs[\Bigg]{\int_1^{\infty} \frac{A_S(x) - \delta A_T(x)}{x^{s+1}} \, dx} \leq \int_1^{\infty} \frac{\varepsilon A_T(x) + C}{x^{s+1}} \, dx = \varepsilon \int_1^{\infty} \frac{A_T(x)}{x^{s+1}} \, dx + \frac{C}{s}.
\]
Using this and the second assumption (that implies that $\lim_{s \to 1^+} \int_1^{\infty} \frac{A_T(x)}{x^{s+1}} \, dx = \infty$) we see that
\[
    \limsup_{s \to 1^+} \abs[\Bigg]{\frac{\int_1^{\infty} \frac{A_S(x) - \delta A_T(x)}{x^{s+1}} \, dx}{\int_1^{\infty} \frac{A_T(x)}{x^{s+1}} \, dx}} \leq \varepsilon
\]
and letting $\varepsilon$ vary we win.

\subsection{}

\begin{task}
    Prove that the natural density of the set
    \[
        S \coloneqq \set{n \in \ZZ_{>0}}{\text{leading digit of $n$ is $1$}}
    \]
    in $T \coloneqq \ZZ_{>0}$ does not exist.
\end{task}

\begin{proof}
    Note that we have
    \[
        [10^k, 2 \cdot 10^k) \subseteq S_{\leq 2 \cdot 10^k}
    \]
    for all $k \in \ZZ_{\geq 0}$.
    It follows that
    \[
        \limsup_{n \to \infty} \frac{\abs{S_{\leq n}}}{\abs{T_{\leq n}}} \geq 1/2.
    \]
    On the other hand we also have
    \[
        S_{\leq 10^{k+1}} \subseteq [1, 2 \cdot 10^{k-1}) \cup \curlybr{10^{k+1}},
    \]
    again for all $k \in \ZZ_{\geq 0}$.
    From this it follows that
    \[
        \liminf_{n \to \infty} \frac{\abs{S_{\leq n}}}{\abs{T_{\leq n}}} \leq 1/5.
    \]
    In particular we see that the natural density of $S$ in $T$ does not exist.
\end{proof}