\section{}

\begin{task}
    Let $G$ be a profinite group and let $n \in \ZZ_{\geq 0}$.
    Show that any map of topological groups $G \to \GL_n(\CC)$ has finite image.
\end{task}

The crucial point is the following lemma.

\begin{lemma}\label{lem:opn-nbhd-GL-C}
    There exists an open neighborhood $U \subseteq \GL_n(\CC)$ of $1$ that does not contain any non-trivial subgroup.
\end{lemma}

\begin{proof}
    Define $U$ to be the set of all matrices $A \in \GL_n(\CC)$ with the following properties:
    \begin{itemize}
        \item
        All eigenvalues of $A$ lie in the (open) set $D \coloneqq \set[\big]{z \in \CC}{\abs{z-1} < 1} \subseteq \CC^{\times}$.

        \item
        We have $\abs{A} < 2$ (here $\abs{\blank}$ denotes the maximum of the absolute values of the matrix entries).
    \end{itemize}
    Both conditions are open so that $U$ is an open neighborhood of $1$ in $\GL_n(\CC)$.
    Now suppose that $A \in \GL_n(\CC)$ such that $A^n \in U$ for all $n \in \ZZ_{\geq 0}$.
    As the eigenvalues of $A^n$ (for some $n \in \ZZ$) are precisely the $n$-th powers of the eigenvalues of $A$ and $D$ contains no nontrivial subgroups of $\CC^{\times}$ we see that all eigenvalues of $A$ have to be $1$.
    Now if the Jordan canonical form of $A$ would contain a non-trivial Jordan block then the powers of $A$ would form an unbounded sequence in $\GL_n(\CC)$.
    But this is not possible as the set $U$ is bounded.

    Thus we have $A = 1$ and are done.
\end{proof}

\begin{lemma}\label{lem:opn-nbhd-profinite}
    Every open neighborhood $V \subseteq G$ of $1$ contains an open subgroup $H \subseteq G$.
\end{lemma}

\begin{proof}
    Without loss of generality we may assume that $G = \prod_{i \in I} G_i$ is a product of finite groups $G_i$.
    Then, by definition of the product topology, after shrinking $V$ we may assume that $V = \prod_{i \in I} V_i$ where $V_i \subseteq G_i$ are some subsets containing $1$ and $V_i = G_i$ for all but finitely many $i$.
    Now define
    \[
        H_i \coloneqq
        \begin{cases}
            G_i &V_i = G_i, \\
            \curlybr{1} &V_i \neq G_i.
        \end{cases}
    \]
    Then $H \coloneqq \prod_{i \in I} H_i$ is an open subgroup of $G$ that is contained in $V$.
\end{proof}

Now let $U$ be an open neighborhood $U \subseteq \GL_n(\CC)$ of $1$ as in \cref{lem:opn-nbhd-GL-C} and let $f \colon G \to \GL_n(\CC)$ be a map of topological groups.
Then $f^{-1}(U)$ is an open neighborhood of $1$ in $G$ and thus contains an open subgroup $H \subseteq G$ by \cref{lem:opn-nbhd-profinite}.
Then $H$ maps under $f$ to a subgroup of $\GL_n(\CC)$ that is contained in $U$ and thus trivial.
Hence $H$ is contained in the kernel of $f$ and $f$ factors over $G/H$ which is a finite group.