\section{}

\subsection{}

Let $((I, \leq), (G_i)_i, (f^j_i)_{ij})$ be an inverse system of finite discrete groups.
Then $G \coloneqq \lim_i G_i$ is a topological group and comes equipped with structure maps (that are maps of topological groups) $\alpha_i \colon G \to G_i$ for all $i \in I$ such that $f^j_i \circ \alpha_j = \alpha_i$ for all $i, j \in I$ with $i \leq j$.
It is the universal topological group with such structure in the following sense:

Suppose that $H$ is a topological group and that we have maps of topological groups $\beta_i \colon H \to G_i$ for all $i$ such that $f^j_i \circ \beta_j = \beta_i$ for all $i, j \in I$ with $i \leq j$.
Then there exists a unique map of topological groups $\beta \colon H \to G$ such that $\beta_i = \alpha_i \circ \beta$ for all $i \in I$.

\subsection{}

\begin{task}
    Show that $(\ZZ_{>0}, \mid)$ is a directed set.
\end{task}

\begin{proof}
    $\ZZ_{>0}$ is non-empty.
    The relation $\mid$ clearly is reflexive and transitive.
    Finally for elements $n, m \in \ZZ_{>0}$ we have $n \mid nm$ and $m \mid nm$.
\end{proof}

\subsection{}

Consider the inverse system $((\ZZ_{>0}, \mid), (\ZZ/n\ZZ)_n, (f^n_m)_{n, m})$ where the transition maps are given by the canonical projections $f^n_m \colon \ZZ/n\ZZ \to \ZZ/m\ZZ$.
Define $\Zhat \coloneqq \varprojlim_n \ZZ/n\ZZ$.

\begin{task}
    Show that there exists a canonical isomorphism
    \[
    \Zhat \to \prod_p \Zp.
    \]
\end{task}

\begin{proof}
    We have the obvious map
    \[
    \Zhat \to \prod_p \Zp, \qquad (x_n)_n \mapsto ((x_{p^k})_k)_p
    \]
    and we claim that this is an isomorphism.
    By the universal property of inverse limits and products the map is automatically continuous.
    As a continuous bijection between compact Hausdorff spaces is automatically a homeomorphism we now only need to show injectivity and surjectivity.
    
    The key is now the Chinese Remainder Theorem:
    If $n \in \ZZ_{>0}$ then the natural map
    \[
    \ZZ/n\ZZ \to \prod_p \ZZ/p^{\nu_p(n)}\ZZ
    \]
    is an isomorphism of finite groups.

    For injectivity suppose that $(x_n)_n \in \Zhat$ is an element such that $x_{p^k} = 0$ for all $p$ prime and $k \in \ZZ_{\geq 0}$.
    Then, for $n \in \ZZ_{>0}$, the element $x_n$ maps to zero under $\ZZ/n\ZZ \to \prod_p \ZZ/p^{\nu_p(n)}\ZZ$ hence is zero itself.
    
    For surjectivity suppose we are given a elements $(y_k^{(p)})_k \in \Zp$ for all primes $p$.
    For $n \in \ZZ_{>0}$ we then define $x_n$ to be the unique preimage of $(y_{\nu_p(n)}^{(p)})_p$ under $\ZZ/n\ZZ \to \prod_p \ZZ/p^{\nu_p(n)}\ZZ$.
    Then $(x_n)_n$ forms a compatible family because for $n, m \in \ZZ_{>0}$ with $m \mid n$ the diagram
    \[
    \begin{tikzcd}
        \ZZ/n\ZZ \arrow{r} \arrow{d}
        &\prod_p \ZZ/p^{\nu_p(n)}\ZZ \arrow{d}
        \\
        \ZZ/m\ZZ \arrow{r}
        &\prod_p \ZZ/p^{\nu_p(m)}\ZZ
    \end{tikzcd}
    \]
    is commutative.
    Finally we clearly have $x_{p^k} = y_k^{(p)}$ for all primes $p$ and $k \in \ZZ_{\geq 0}$ so that $(x_n)_n$ is a preimage.
\end{proof}

\subsection{}

The isomorphism in the third part of the exercise actually is an isomorphism of rings.
This follows formally because the maps $\Zhat \to \Zp$ are obtained from restricting the index set of the inverse limit from $\ZZ_{>0}$ to only the powers of $p$.
