These are my solutions to the exercises from the ANT2 lecture held by Dr. Johannes Sprang at the University Duisburg-Essen.

[Sheet 01](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_01.pdf)

[Sheet 02](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_02.pdf)

[Sheet 03](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_03.pdf)

[Sheet 04](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_04.pdf)

[Sheet 05](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_05.pdf)

[Sheet 06](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_06.pdf)

[Sheet 07](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_07.pdf)

[Sheet 08](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_08.pdf)

[Sheet 09](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_09.pdf)

[Sheet 10](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_10.pdf)

[Sheet 11](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_11.pdf)

[Sheet 12](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_12.pdf)

[Sheet 13](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_13.pdf)

[Sheet 14](https://makoba.gitlab.io/ss2021-algebraic-number-theory-2/sheet_14.pdf)
