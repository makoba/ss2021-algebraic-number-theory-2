\section{}

\begin{task}
    Let $e$ be the ramification index of $K/\Qp$ (i.e.\ the absolute ramification index of $K$).
    For $n > \frac{e}{p-1}$ show that the exponential map
    \[
    \exp \colon \frakm^n \to U^{(n)}, \qquad x \mapsto \sum_{k=0}^{\infty} \frac{x^k}{k!}
    \]
    is a well-defined isomorphism of abelian groups $(\frakm^n, +) \cong (U^{(n)}, \cdot)$.
\end{task}

First we need a description of $\nu_p(k!)$

\begin{claim}
    For $k \in \ZZ_{\geq 0}$ we have
    \[
    \nu_p(k!) = \frac{k - s_p(k)}{p-1}.
    \]
    Here $s_p(k) \in \ZZ_{\geq 0}$ denotes $\sum_{i = 0}^{\infty} a_i$ if $k = \sum_{i = 0}^{\infty} a_i p^i$ with $a_i \in \curlybr{0, \dotsc, p-1}$.
\end{claim}

\begin{proof}
    We can compute
    \begin{align*}
        \nu_p(k!) &= \sum_{i = 1}^{\infty} \floor[\Big]{\frac{k}{p^i}} \\
        &= \sum_{i = 1}^{\infty} \sum_{j = 0}^{\infty} a_{i+j} p^j \\
        &= \sum_{l=1}^{\infty} \sum_{j = 0}^{l-1} a_l p^j \\
        &= \sum_{l=1}^{\infty} a_l \frac{p^l - 1}{p - 1} \\
        &= \frac{k - s_p(k)}{p - 1}
    \end{align*}
    using the formula that we already have proven on some other exercise sheet.
\end{proof}

\begin{claim}
    We make the following claims:
    \begin{itemize}
        \item
        Let $x \in \frakm^n$.
        The series $\exp(x)$ converges in $K$ and for all $k \geq 1$ we have $\frac{x^k}{k!} \in \frakm^n$ so that $\exp(x) \in U^{(n)}$ as claimed.
        
        \item
        Let $x \in \frakm^n$.
        The series $\log(1 + x) \coloneqq \sum_{k=1}^{\infty} (-1)^{k-1} \frac{x^k}{k}$ converges in $K$ and for all $k \geq 1$ we have $\frac{x^k}{k} \in \frakm^n$ so that $\log(1+x) \in \frakm^n$.
        
        \item
        $\exp$ and $\log$ define mutually inverse isomorphisms of abelian groups.
    \end{itemize}
\end{claim}

\begin{proof}
    First note that the second part follows as soon as we have proven the first part.
    
    For the first part we start with computing
    \begin{align*}
        \nu_p(x^k/k!) &= k \nu_p(x) - \frac{k - s_p(k)}{p - 1} \\
        &\geq \nu_p(x) + (k-1) \cdot \roundbr[\Big]{\nu_p(x) - \frac{1}{p-1}}
    \end{align*}
    for $k \geq 1$ (where we have used that $s_p(k) \geq 1$).
    The last term now clearly converges to $\infty$ for $k \to \infty$ (using that $n > \frac{e}{p-1}$) and is bigger or equal than $\nu_p(x)$.
    
    The three identities $\exp(x + y) = \exp(x) \exp(y)$ and $\log(\exp(x)) = x$ and $\exp(\log(1 + x)) = 1 + x$ for $x, y \in \frakm^n$ now follow formally from the corresponding identites of formal power series.
\end{proof}

Now we also want to turn $U^{(1)}$ into a $\Zp$-module such that $U^{(k)} \subseteq U^{(1)}$ is a submodule for all $k \geq 1$ and show that for $n > \frac{e}{p-1}$ the map $\exp$ is actually an isomorphism of $\Zp$-modules.

For this we first note that
\[
U^{(1)} \cong \varprojlim_{k \geq 1} U^{(1)}/U^{(k)}
\]
and that the terms in the limit are all finite abelian groups of $p$-power order.
Thus all these terms (and consequently also the limit) naturally carry the structure of a $\Zp$-module and we can define a $\Zp$-module structure on $U^{(1)}$ by requiring that the above isomorphism is an isomorphism of $\Zp$-modules.
The $U^{(k)}$ for $k \geq 1$ are then simply the kernels of the $\Zp$-linear projection maps $U^{(1)} \to U^{(1)}/U^{(k)}$ and thus $\Zp$-submodules.

To see that $\exp$ is $\Zp$-linear we now note that it induces isomorphisms of abelian groups of $p$-power order $\frakm^n/\frakm^k \cong U^{(n)}/U^{(k)}$ for all $k \geq n$ that are automatically $\Zp$-linear and that one can recover the isomorphism $\frakm^n \cong U^{(n)}$ by passing to the limit of these isomorphisms.
