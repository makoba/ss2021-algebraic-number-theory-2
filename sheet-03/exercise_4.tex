\section{}

\begin{task}
    Prove Bertrand's Postulate.
    Show that for each $n \in \ZZ_{>0}$ there is a prime in the interval $(n, 2n]$.
\end{task}

We first show a few claims:

\begin{claim}
    Let $n \in \ZZ_{>0}$.
    Then we have $\frac{4^n}{2n + 1} \leq \binom{2n}{n}$.
\end{claim}

\begin{proof}
    We compute
    \[
    4^n = (1 + 1)^{2n} = \sum_{k = 0}^{2n} \binom{2n}{k} \leq (2n + 1) \binom{2n}{n}. \qedhere
    \]
\end{proof}

\begin{claim}
    Let $n \in \ZZ_{\geq 0}$ and let $p$ be a prime.
    Then we have
    \[
    \nu_p(n!) = \sum_{i = 1}^{\infty} \floor[\Big]{\frac{n}{p^i}}.
    \]
\end{claim}

\begin{proof}
    For $i \in \ZZ_{\geq 1}$ define
    \[
    X_i \coloneqq \set[\big]{k \in [1, n]}{\nu_p(k) \geq i}.
    \]
    Then the $X_i$ are finite sets that are empty for large $i$.
    We also get
    \begin{align*}
        \nu_p(n!) &= \sum_{k = 1}^n \nu_p(k) \\
        &= \sum_{i = 1}^{\infty} i \cdot \abs{X_i \setminus X_{i+1}} \\
        &= \sum_{i = 1}^{\infty} \abs{X_i}.
    \end{align*}
    Now it suffices to observe that for $i \in \ZZ_{\geq 1}$ the elements of $X_i$ are precisely the positive multiples of $p^i$ that are smaller or equal than $n$ so that $\abs{X_i} = \floor[\big]{\frac{n}{p^i}}$.
\end{proof}

\begin{claim}
    Let $n \in \ZZ_{> 0}$
    Then, for all primes $p$ we have
    \[
    p^{\nu_p \binom{2n}{n}} \leq 2n.
    \]
    In particular $\nu_p \binom{2n}{n} \leq 1$ for $p > \sqrt{2n}$.
    We also have $\nu_p \binom{2n}{n} = 0$ for $p \in (2n/3, n]$ and $n \geq 5$.
\end{claim}

\begin{proof}
    We have
    \[
    \nu_p \binom{2n}{n} = \nu_p((2n)!) - 2 \nu_p(n!) = \sum_{i = 1}^{\infty} \floor[\Big]{\frac{2n}{p^i}} - 2 \floor[\Big]{\frac{n}{p^i}}.
    \]
    Now each of the terms $\floor[\big]{\frac{2n}{p^i}} - 2 \floor[\big]{\frac{n}{p^i}}$ is either $0$ or $1$ and it certainly is $0$ when $p^i > 2n$.
    This shows the first part of the claim (and the second one readily follows).
    
    For the third part of the claim we note that the assumptions imply that $2 \leq \frac{2n}{p} < 3$ so that $\floor[\big]{\frac{2n}{p}} - 2 \floor[\big]{\frac{n}{p}} = 0$ and that $p^2 > 2n$ (this uses that $n \geq 5$).
\end{proof}

Now we are ready to prove Bertrand's Postulate.

\begin{proof}
    First we can verify that we have a sequence of prime numbers
    \[
    2, 3, 5, 7, 13, 23, 43, 83, 163, 317, 631
    \]
    starting with $2$ and where each term is bigger than half of the next term.
    This proves Bertrand's Postulate for all $n \leq 630$.
    
    Now assume that $n \geq 631$ such that there exists no prime $p$ in $(n, 2n]$.
    Then we can bound
    \[
    \binom{2n}{n} \leq (2n)^{\sqrt{2n}} \prod_{\sqrt{2n} < p \leq 2n/3} p \leq (2n)^{\sqrt{2n}} \prod_{p \leq 2n/3} p < (2n)^{\sqrt{2n}} 4^{2n/3}.
    \]
    Here we have used the following observations (from previous claims):
    \begin{itemize}
        \item
        No prime number bigger than $2n$ or in $(2n/3, n]$ divides $\binom{2n}{n}$.
        
        \item
        Every prime power dividing $\binom{2n}{n}$ is at most $2n$.
        
        \item
        Every prime number in $(\sqrt{2n}, 2n/3]$ divides $\binom{2n}{n}$ at most once.
        
        \item
        In the lecture we have seen that
        \[
        \prod_{p \leq k \, \text{prime}} p < 4^k
        \]
        for any $k \in \ZZ_{\geq 2}$.
    \end{itemize}
    Using the very first claim we now get the inequality
    \[
    \frac{4^n}{2n+1} \leq \binom{2n}{n} \leq (2n)^{\sqrt{2n}} 4^{2n/3}.
    \]
    But, \enquote{using calculus} and maybe a computer, this inequality can be seen to be false (remember that $n \geq 631$), leading to a contradiction.
\end{proof}
