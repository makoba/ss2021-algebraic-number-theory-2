\section{}

\begin{task}
    Show that we have
    \[
    B_{2n} + \sum_{(p-1) \mid 2n} \frac{1}{p} \in \ZZ
    \]
    for all $n \in \ZZ_{> 0}$.
    In particular the primes dividing the denominator of $B_{2n}$ are exactly the primes such that $p-1$ divides $2n$.
\end{task}

Fix $n \in \ZZ_{>0}$.
Our strategy is to first prove the statement locally at each prime and then deduce the global result.
So for now, also fix a prime $p$.

\begin{claim}
    We have $B_{2n} = \lim_{s \to \infty} S_{2n}(p^s-1)/p^s$ where the limit is taken with respect to the $p$-adic topology on $\QQ$.
\end{claim}

\begin{proof}
    Using the previous exercise we observe that
    \begin{align*}
        S_{2n}(p^s-1)/p^s &= \sum_{m=0}^{2n} \binom{2n}{m} \frac{B_m}{2n+1-m} p^{s(2n-m)} \\
        &= B_{2n} + \sum_{m=0}^{2n-1} \binom{2n}{m} \frac{B_m}{2n+1-m} p^{s(2n-m)}
    \end{align*}
    for $s \in \ZZ_{>0}$.
    The term $\sum_{m=0}^{2n-1} \binom{2n}{m} \frac{B_m}{2n+1-m} p^{s(2n-m)}$ in the second line now clearly converges to zero $p$-adically for $s \to \infty$ so that we can deduce our claim.
\end{proof}

\begin{claim}
    For $s \in \ZZ_{>0}$ we have $\frac{S_{2n}(p^{s+1}-1)}{p^{s+1}} - \frac{S_{2n}(p^s-1)}{p^s} \in \ZZ_{(p)}$.
\end{claim}

\begin{proof}
    We have
    \begin{align*}
        S_{2n}(p^{s+1}-1) &= \sum_{j=0}^{p^{s+1}-1} j^{2n} = \sum_{u=0}^{p-1} \sum_{v=0}^{p^s-1} (up^s+v)^{2n} \\
        &\equiv p \sum_{v=0}^{p^s-1} v^{2n} + 2np^s \sum_{u=0}^{p-1} \sum_{v=0}^{p^s-1} uv^{2n-1} \\
        &= p S_{2n}(p^s-1) + 2np^s \roundbr[\Bigg]{\sum_{u=0}^{p-1} u} \cdot \roundbr[\Bigg]{\sum_{v=0}^{p^s-1} v^{2n-1}}
    \end{align*}
    where the congruence is modulo $p^{2s}$.
    Now note that we have $2 \sum_{u=0}^{p-1} u \equiv 0$ module $p$ (the factor $2$ is necessary to deal with the case $p = 2$) so that we in fact obtain $S_{2n}(p^{s+1}-1) \equiv p S_{2n}(p^s-1)$ modulo $p^{s+1}$.
    By dividing this by $p^{s+1}$ we get the claim (and even that the difference is contained in $\ZZ$).
\end{proof}

\begin{claim}
    We have
    \[
    S_{2n}(p-1) \equiv \begin{cases}-1 &(p-1) \mid 2n \\ 0 &(p-1) \nmid 2n\end{cases} \pmod{p}
    \]
\end{claim}

\begin{proof}
    If $(p-1) \mid 2n$ then $j^{2n} \equiv 1$ modulo $p$ for all $j = 1, \dotsc, p-1$ so that we get $S_{2n}(p-1) \equiv p-1 \equiv -1$ as desired.
    Now suppose that $(p-1) \nmid 2n$ and let $g \in \ZZ$ be a representative of some generator of $\Fp^{\times}$.
    Then we have
    \[
    S_{2n}(p-1) \equiv \sum_{t=1}^{p-1} g^t \equiv  g \sum_{t=0}^{p-2} g^t \equiv g S_{2n}(p-1)
    \]
    modulo $p$.
    Dividing by $g - 1$ in the field $\Fp$ yields the desired claim.
\end{proof}

Now we put everything together.
By the first claim the series $(S_{2n}(p^s-1)/p^s)_{s > 0}$ converges to $B_{2n}$ in the $p$-adic topology so that in particular there exists some $s$ such tht $B_{2n} - S_{2n}(p^s-1)/p^s \in \ZZ_{(p)}$.
Applying the second claim repeatedly we then obtain that actually already $B_{2n} - S_{2n}(p-1)/p \in \ZZ_{(p)}$.
The third claim says precisely that $(S_{2n}(p-1) + 1)/p$ (resp.\ $S_{2n}(p-1)/p$) is contained in $\ZZ_{(p)}$ provided that $(p-1) \mid 2n$ (resp.\ $(p-1) \nmid 2n$).
Applying this we obtain that $B_{2n} + 1/p$ (resp.\ $B_{2n}$) is contained in $\ZZ_{(p)}$.
As we have $1/q \in \ZZ_{(p)}$ for all prime numbers $q \neq p$ it follows that
\[
B_{2n} + \sum_{(q-1) \mid 2n} \frac{1}{q} \in \ZZ_{(p)}.
\]
Letting $p$ vary and using that $\bigcap_p \ZZ_{(p)} = \ZZ$ we obtain the desired result.
